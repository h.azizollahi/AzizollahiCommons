/*
Copyright (C) 2015 - 2016 Hossein Azizollahi. All rights reserved.
 */


/* 
 * File:   Service.h
 * Author: azizollahi
 *
 * Created on March 1, 2017, 10:58 AM
 */

#ifndef SERVICE_H
#define SERVICE_H

#include<exception>
#include <memory>
#include "Agent.h"

namespace Azizollahi::Commons{
	class Service {
	public:
		virtual ~Service(){};

		virtual void start(Agent*) = 0;
		virtual void stop() = 0;

	protected:
	};
}



#endif /* SERVICE_H */

