/*
Copyright (C) 2015 - 2016 Hossein Azizollahi. All rights reserved.
 */


/* 
 * File:   PeriodicService.h
 * Author: azizollahi
 *
 * Created on March 2, 2017, 10:18 PM
 */

#ifndef PERIODICSERVICE_H
#define PERIODICSERVICE_H

#include"Service.hpp"
#include<thread>
#include<chrono>

namespace Azizollahi::Commons{
	class PeriodicService : public Service {
	public:
		PeriodicService(int interval);
		PeriodicService(const PeriodicService& orig);
		virtual ~PeriodicService();
		void start(Agent*) override;
		void stop() override;
		std::string toString();
	protected:
	private:
		void static cycle(Agent* agent, bool* isWorking, int* interval);
		std::thread* _workingThread;
		bool* _isWorking;
		int* _interval;
	protected:
	};
}


#endif /* PERIODICSERVICE_H */

