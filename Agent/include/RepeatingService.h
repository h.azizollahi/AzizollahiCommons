/*
Copyright (C) 2015 - 2016 Hossein Azizollahi. All rights reserved.
 */


/* 
 * File:   RepeatingService.h
 * Author: azizollahi
 *
 * Created on March 1, 2017, 9:45 PM
 */

#ifndef REPEATINGSERVICE_H
#define REPEATINGSERVICE_H

#include"Service.hpp"

#include<mutex>
#include<chrono>
#include<thread>

namespace Azizollahi::Commons {
	class RepeatingService : public Service{
	public:
		RepeatingService(const RepeatingService& orig);
		virtual ~RepeatingService();
		RepeatingService();
		void start(Agent*) override;
		void stop() override;

	protected:
	protected:
	private:
		void static cycle(Agent* agent, bool* isWorking);
		std::thread* _workingThread;
		bool* _isWorking;
	};
}


#endif /* REPEATINGSERVICE_H */

