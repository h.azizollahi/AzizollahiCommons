//
// Created by Developer on 3/29/2018.
//

#ifndef AZIZOLLAHICOMMONS_AGENT_H
#define AZIZOLLAHICOMMONS_AGENT_H


#include <string>

namespace Azizollahi::Commons {
	class Agent {
	protected:
		std::string name;

	public:
		virtual ~Agent();
		virtual bool doJob() = 0;

		virtual std::string getName() = 0;

	};
}


#endif //AZIZOLLAHICOMMONS_AGENT_H
