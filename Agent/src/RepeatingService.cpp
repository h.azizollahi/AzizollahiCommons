/*
Copyright (C) 2015 - 2016 Hossein Azizollahi. All rights reserved.
 */


/* 
 * File:   RepeatingService.cpp
 * Author: azizollahi
 * 
 * Created on March 1, 2017, 9:45 PM
 */

#include "../Include/RepeatingService.h"

using namespace Azizollahi::Commons;

RepeatingService::RepeatingService() {
	_workingThread = nullptr;
	_isWorking = new bool();
}

RepeatingService::RepeatingService(const RepeatingService& orig) {
	*this = orig;
}

RepeatingService::~RepeatingService() {
	delete _isWorking;
}

void RepeatingService::start(Agent* agent){
	stop();
	*_isWorking = true;
	_workingThread = new std::thread(cycle,agent, _isWorking);
	_workingThread->detach();
}

void RepeatingService::stop(){
	*_isWorking = false;
	if(_workingThread == nullptr)
		return;
	delete _workingThread;
}

void RepeatingService::cycle(Agent* agent, bool* isWorking){
	while(*isWorking) {
		try{
			if(!agent->doJob())
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
		catch(...){
			return;
		}
	}
}

