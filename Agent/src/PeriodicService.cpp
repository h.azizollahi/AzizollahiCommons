/*
Copyright (C) 2015 - 2016 Hossein Azizollahi. All rights reserved.
 */


/* 
 * File:   PeriodicService.cpp
 * Author: azizollahi
 * 
 * Created on March 2, 2017, 10:18 PM
 */

#include "../Include/PeriodicService.h"

using namespace Azizollahi::Commons;

PeriodicService::PeriodicService(int interval) {
	_interval = new int();
	*_interval = interval;
	_workingThread = nullptr;
	_isWorking = new bool();
}

PeriodicService::PeriodicService(const PeriodicService& orig) {
	*this = orig;
}

PeriodicService::~PeriodicService() {
	delete _isWorking;
	delete _interval;
}

std::string PeriodicService::toString(){
	return "PeriodicService";
}

void PeriodicService::start(Agent* agent){
	stop();
	*_isWorking = true;
	_workingThread = new std::thread(cycle,agent,_isWorking, _interval);
	_workingThread->detach();
}

void PeriodicService::stop(){
	*_isWorking = false;
	if(_workingThread == nullptr)
		return;
	delete _workingThread;
}

void PeriodicService::cycle(Agent* agent, bool* isWorking, int* interval){
	while(*isWorking) {
		try{
			std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
			
			bool chkJob = agent->doJob();
			
			std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed_seconds = end-start;
			int elapsed = (int)elapsed_seconds.count();
			if(chkJob)
			{
				if(elapsed < *interval)
				{
					int leftTime = *interval - elapsed;
					std::this_thread::sleep_for(std::chrono::milliseconds(leftTime));
				}
			}
			else
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(*interval));
			}
		}
		catch(...){
			return;
		}
	}
}

